import java.util.Scanner; 

public class Monate {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte gebe eine Zahl zwischen 1 und 12 für den Monat ein:");
		
		int monat = scan.nextInt();
		
		scan.close();

		while (monat >= 1) { 
			if (monat == 1) {
				System.out.println("Januar");
				break;
			}
			else if (monat == 2) {
				System.out.println("Februar");
				break;
			}
			else if (monat == 3) {
				System.out.println("März");
				break;
			}
			else if (monat == 4) {
				System.out.println("April");
				break;
			}
			else if (monat == 5) {
				System.out.println("Mai");
				break;
			}
			else if (monat == 6) {
				System.out.println("Juni");
				break;
			}
			else if (monat == 7) {
				System.out.println("Juli");
				break;
			}
			else if (monat == 8) {
				System.out.println("August");
				break;
			}
			else if (monat == 9) {
				System.out.println("September");
				break;
			}
			else if (monat == 10) {
				System.out.println("Oktober");
				break;
			}
			else if (monat == 11) {
				System.out.println("November");
				break;
			}
			else if (monat == 12) {
				System.out.println("Dezember");
				break;
			}
			else {
				System.out.println("Das war keine Zahl zwischen 1 und 12!");
				break;
			}
		}
		System.out.println("\nDanke!");
	}

}
