import java.util.Scanner;

public class FahrkartenautomatTicketbegrenzung {

	public static void main(String[] args) {

				Scanner tastatur = new Scanner(System.in);

				double zuZahlenderBetrag;
				double eingezahlterGesamtbetrag;
				double eingeworfeneMünze;
				double rückgabebetrag;
				double anzahlTicket;

				System.out.print("Zu zahlender Betrag (EURO): ");
				zuZahlenderBetrag = fahrkartenBestellungErfassen();

				System.out.print("Geben sie die gewünschte Anzahl an Tickets an:");
				anzahlTicket = ticketAnzahlErfassen();

				// Geldeinwurf
				// -----------
				eingezahlterGesamtbetrag = 0.0;
				while (eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTicket) {
					System.out.printf("Noch zu zahlen: %.2f Euro",
							(zuZahlenderBetrag * anzahlTicket - eingezahlterGesamtbetrag));
					System.out.printf("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
					eingeworfeneMünze = tastatur.nextDouble();
					eingezahlterGesamtbetrag += eingeworfeneMünze;
				}
				

				System.out.printf("Ihr Wechselgeld beträgt: %.2f",
				fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, anzahlTicket));

				// Fahrscheinausgabe
				// -----------------
				fahrkartenAusgeben();

				// Rückgeldberechnung und -Ausgabe
				// -------------------------------
				rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTicket;
				warte(2500);
				System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro wird in folgenden Münzen ausgezahlt: \n",
						rueckgeldAusgeben(rückgabebetrag));
				muenzeAusgeben(rückgabebetrag);

				warte(1500);
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
						+ "Wir wünschen Ihnen eine gute Fahrt.");
			}

			// Methoden Fahrkartenautomat Aufgabe 3.3
			public static double fahrkartenBestellungErfassen() {
				{
					Scanner myScanner = new Scanner(System.in);
					double fahrkarte = myScanner.nextDouble();
					return fahrkarte;
				}
			}
			
			//Begrenzte Tickets --> Methode (Aufgabe 4.2)
			public static double ticketAnzahlErfassen ()
			{
				Scanner myScanner = new Scanner(System.in);
				double fahrkarte = myScanner.nextDouble();
				if(fahrkarte >= 1 && fahrkarte <= 10)
				{
					
				}
				else
				{
					System.out.println("Die Anzahl " + fahrkarte + " wird nicht zugelassen. Der Wert wird auf '1' Fahrkarte gesetzt!");
					fahrkarte = 1;
				}
				return fahrkarte;
			}

			public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
					double ticketAnzahl) {
				double wechselgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
				return wechselgeld;
			}

			public static void fahrkartenAusgeben() {
				System.out.println("\nFahrschein wird ausgegeben");
				for (int i = 0; i < 8; i++) {
					System.out.print("=");
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println("\n\n");
			}

			public static double rueckgeldAusgeben(double rückgabebetrag) {
				double wechselgeld = rückgabebetrag;
				return wechselgeld;
			}

			//Aufgabe A3.4
			public static void muenzeAusgeben(double rückgabebetrag) {
				if (rückgabebetrag > 0.0) {
					rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					/*
					 * System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ",
					 * rückgabebetrag, " EURO ");
					 * System.out.println("wird in folgenden Münzen ausgezahlt:");
					 */

					while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
					{
						System.out.println("2 EURO");
						rückgabebetrag -= 2.0;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
					while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
					{
						System.out.println("1 EURO");
						rückgabebetrag -= 1.0;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
					while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
					{
						System.out.println("50 CENT");
						rückgabebetrag -= 0.5;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
					while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
					{
						System.out.println("20 CENT");
						rückgabebetrag -= 0.2;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
					while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
					{
						System.out.println("10 CENT");
						rückgabebetrag -= 0.1;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
					while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
					{
						System.out.println("5 CENT");
						rückgabebetrag -= 0.05;
						rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
					}
				}

			}
		}
