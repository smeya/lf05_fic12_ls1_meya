import java.util.Scanner; // Import Scanner... which is slow...

public class Noten {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte gebe eine Zahl zwischen 1 und 6 ein:");
		
		int note = scan.nextInt();
		
		scan.close();

		while (note >= 1) { // && note <= 6
			if (note == 1) {
				System.out.println("1 = Sehr gut");
				break;
			}
			else if (note == 2) {
				System.out.println("2 = Gut");
				break;
			}
			else if (note == 3) {
				System.out.println("3 = Befriedigend");
				break;
			}
			else if (note == 4) {
				System.out.println("4 = Ausreichend");
				break;
			}
			else if (note == 5) {
				System.out.println("5 = Mangelhaft");
				break;
			}
			else if (note == 6) {
				System.out.println("6 = Ungenügend");
				break;
			}
			/* else if (note >= 7) {
				System.out.println("Das war keine Zahl zwischen 1 und 6!");
				break;
			} */
			else {
				System.out.println("Das war keine Zahl zwischen 1 und 6!");
				break;
			}
		}
		System.out.println("\nDanke!");
	}
}