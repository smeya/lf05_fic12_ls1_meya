
import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner s = new Scanner(System.in);

		System.out.println("Geben Sie bitte eine Zahl ein:\n");

		int i;
		int quer = 0;
		int rest;
        int zahl = s.nextInt();       
		s.close();

		while (zahl > 0) {
			rest = zahl % 10;
			quer = quer + rest;
			zahl = zahl / 10;
		}

		System.out.println("Die Quersumme ist: " + quer);
	}

}
