import java.util.Scanner;

public class Gleichheit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("Erste Zahl: ");
		int x = scn.nextInt();
		System.out.println("Zweite Zahl: ");
		int y = scn.nextInt();
		System.out.println("Dritte Zahl: ");
		int z = scn.nextInt();
		
		System.out.println("\nMit 2 Zahlen:\n");
		
		// Aufgabe 2:
		if(x == y) {
			System.out.println("Zahlen sind gleich!\n");
		} else {
			System.out.println("Zahlen sind ungleich!\n");
		}
		// Aufgabe 3:
		if(y > x) {
			System.out.println("Die zweite Zahl ist größer als die erste Zahl!\n");
		} else {
			System.out.println("Die zweite Zahl ist größer oder gleich der ersten Zahl!\n");
		}
		// Aufgabe 4:
		if(x >= y) {
			System.out.println("Die erste Zahl oder größer oder gleich der zweiten Zahl!\n");
		} else {
			System.out.println("Die erste Zahl ist kleiner oder ungleich der zweiten Zahl!\n");
		}
		
		System.out.println("Mit 3 Zahlen:\n");
		
		// Aufgabe 1
		if(x > y && x > z) {
			System.out.println("x ist größer als y und größer als z!\n");
		} else {
			System.out.println("x ist kleiner als y und kleiner als z!\n");
		// Aufgabe 2
		if(z > y || z > x) {
			System.out.println("z ist größer als y oder x!\n");
		} else {
			System.out.println("z ist kleiner als y oder x!\n");
		}
		// Aufgabe 3
		if ( x > y && x > z) {
			System.out.println(x + " ist die größte Zahl!\n");
			}
		else if ( y > x && y > z) {
		    System.out.println(y + " ist die größte Zahl!\n");
		} else {
		    System.out.println(z + " ist die größte Zahl!\n");
			}
		}
	}
		
}
