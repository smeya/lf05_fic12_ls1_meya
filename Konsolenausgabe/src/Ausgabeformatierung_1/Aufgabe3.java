package Ausgabeformatierung_1;
/**
 * @author sm
 */
public class Aufgabe3 {

	public static void main(String[] args) {

		double zwo = 22.4234234;
		double elf = 111.2222;
		double vier = 4.0;
		double mio = 1000000.551;
		double byear = 97.34;
		
		System.out.printf("%.2f%n", zwo);
		System.out.printf("%.4f%n", elf);
		System.out.printf("%.1f%n", vier);
		System.out.printf("%.3f%n", mio);
		System.out.printf("%.2f%n", byear);
	}

}
