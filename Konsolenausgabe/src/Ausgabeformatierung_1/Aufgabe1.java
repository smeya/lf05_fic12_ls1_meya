package Ausgabeformatierung_1;
/**
 * @author sm
 */
public class Aufgabe1 {
	
	public static void main(String[] args) {

		System.out.printf("Das ist ein Beispielsatz. ");
		System.out.printf("Ein Beispielsatz ist das.");
		
		System.out.println("\n");
		
		System.out.println("Das ist ein \"Beispielsatz.\"\nEin Beispielsatz ist das.");

		// Das ist ein Kommentar.
	}

}