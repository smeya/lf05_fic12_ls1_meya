package Ausgabeformatierung_2;

/**
 * @author sm
 */
public class Aufgabe3 {

	public static void main(String[] args) {
		
		// Leiste
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "Celsius");
		System.out.printf("%23s%n", "-----------------------");

		// Erste Zeile
		System.out.printf("%-12s", "-20");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "-28.89");
		
		// Zweite Zeile
		System.out.printf("%-12s", "-10");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "-23.33");

		// Dritte Zeile
		System.out.printf("%-12s", "+0");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "-17.78");
		
		// Vierte Zeile
		System.out.printf("%-12s", "+20");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "-6.67");
				
		// Fünfte Zeile
		System.out.printf("%-12s", "+30");
		System.out.printf("%1s", "|");
		System.out.printf("%10s%n", "-1.11");
	}

}
