package Ausgabeformatierung_2;
/**
 * @author sm
 */
public class Aufgabe2 {

	/**
	 * @param args
	 */
	
	static void calcFak(int zahl) {
		int fak = 1;
		
		for (int i = 1; i <= zahl; ++i) {
			fak = fak * i;
		}
		
		System.out.printf("%4s\n", fak);
	}
	public static void main(String[] args) {
		
		// Erste Zeile
		System.out.printf("%-5s", "0!");
		System.out.printf("%s", "=");
		System.out.printf("%20s", "=");
		//System.out.printf("%5s\n", "1");
		calcFak(0);
		
		// Zweite Zeile
		System.out.printf("%-5s", "1!");
		System.out.printf("%s", "= 1");
		System.out.printf("%18s", "=");
		//System.out.printf("%5s\n", "1");
		calcFak(1);
		
		// Dritte Zeile
		System.out.printf("%-5s", "2!");
		System.out.printf("%s", "= 1 * 2");
		System.out.printf("%14s", "=");
		//System.out.printf("%5s\n", "2");
		calcFak(2);
		
		// Vierte Zeile
		System.out.printf("%-5s", "3!");
		System.out.printf("%s", "= 1 * 2 * 3");
		System.out.printf("%10s", "=");
		//System.out.printf("%5s\n", "6");
		calcFak(3);
		
		// Fünfte Zeile
		System.out.printf("%-5s", "4!");
		System.out.printf("%s", "= 1 * 2 * 3 * 4");
		System.out.printf("%6s", "=");
		//System.out.printf("%5s\n", "24");
		calcFak(4);
		// Fünfte Zeile
		System.out.printf("%-5s", "5!");
		System.out.printf("%s", "= 1 * 2 * 3 * 4 * 5 =");
		//System.out.printf("%2s", "=");
		//System.out.printf("%5s\n", "120");
		calcFak(5);
	}

}
