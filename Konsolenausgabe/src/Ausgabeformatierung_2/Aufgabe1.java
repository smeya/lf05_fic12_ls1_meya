package Ausgabeformatierung_2;

/**
 * @author sm
 */
public class Aufgabe1 {

	public static void main(String[] args) {

		String s = "*";
		
		System.out.printf("%7s%n", "**");
		System.out.printf("%3s", s);
		System.out.printf("%7s%n", s);
		System.out.printf("%3s", s);
		System.out.printf("%7s%n", s);
		System.out.printf("%7s", "**");
	}

}
