public class Aufgabe4 {

	public static void main(String[] args) {

        int[] arr = {3, 7, 12, 18, 37, 42};
    	boolean ist_da = false;

        for (int i = 0; i < 6; i++) {
        	
            System.out.print(arr[i]);
            System.out.print(" ");
            
        }
        
        System.out.println("\nTeste, ob 12 oder 13 enthalten ist:");

        for (int i = 0; i < 6; i++) {
            
            if (arr[i] == 12) {
            	System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
            }
            
            if (arr[i] != 13 && ist_da == false) {
            	System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
            	ist_da = true;
            }
        }

	}
}