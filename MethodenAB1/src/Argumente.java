public class Argumente { 
 public static void main(String[] args) { 
  ausgabe(1, "Mana"); 
  ausgabe(2, "Elise"); 
  ausgabe(3, "Johanna"); 
  ausgabe(4, "Felizitas"); 
  ausgabe(5, "Karla"); 
  System.out.println(vergleichen(1, 2)); 
  System.out.println(vergleichen(1, 5)); 
  System.out.println(vergleichen(3, 4)); 
 } 
 // gibt eine Zahl und einen Namen aus
 public static void ausgabe(int zahl, String name) { 
  System.out.println(zahl + ": " + name); 
 } 
 
 // vergleicht, ob Zahl 1 + 8 kleiner als Zahl 3 * 3 ist
 public static boolean vergleichen(int arg1, int arg2) { 
  return (arg1 + 8) < (arg2 * 3); 
 } 
}