
public class Volumenberechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Wuerfel();
		Quader();
		Pyramide();
		Kugel();
	}
	public static int Wuerfel() {
		// Variablen zuweisen
		int a = 5;
		
		// Ausgabe des Ergebnis
		System.out.println("Volumen des Würfels in cm³: " + a * a * a);
		return 0;
	}
	public static int Quader() {
		// Variablen zuweisen
		int a = 5;
		int b = 3;
		int c = 6;
		
		// Ausgabe des Ergebnis
		System.out.println("Volumen des Quaders in cm³: " + a * b * c);
		return 0;
	}
	public static int Pyramide() {
		// Variablen zuweisen
		int a = 5;
		int h = 6;
		
		// Ausgabe des Ergebnis
		System.out.println("Volumen der Pyramide in cm³: " + (a * a * h) / 3);
		return 0;
	}
	public static double Kugel() {
		// Variablen zuweisen
		int r = 5;
		double pi = 3.14159;
		
		// Ausgabe des Ergebnis
		System.out.println("Volumen der Kugel in cm³: " + (4/3 * (r * r * r) * pi));
		return 0;
	}
}
